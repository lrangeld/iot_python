import os
import json
from os.path import join, dirname
from watson_developer_cloud import SpeechToTextV1 as SpeechToText

from stt.recorder import Recorder

def transcribe_audio(path_to_audio_file):
    speech_to_text = SpeechToText(username="eb7af922-e695-4689-b424-17324b798a77", password="kpOVFe7AMKJq")

    with open(join(dirname(__file__), path_to_audio_file), 'rb') as audio_file:
        return speech_to_text.recognize(audio_file, content_type='audio/wav', model='es-ES_NarrowbandModel')

def main():
    recorder = Recorder("audio.wav")

    print("Diga algo al microfono...\n")
    recorder.record_to_file()

    print("Transcribiendo audio....\n")
    result = transcribe_audio('audio.wav')
    
    text = result['results'][0]['alternatives'][0]['transcript']
    print("Texto: " + text + "\n")

if __name__ == '__main__':
    try:
        main()
    except:
        print("IOError...")
        main()



from flask import Flask,redirect
from flask import render_template
from flask import request
import json
import time
import ibmiotf.application
import sys
import uuid
from uuid import getnode as get_mac
import ibmiotf.device

organization = "kon9yz"
deviceType = "raspi-iot-device"
deviceId = "rangelpi-iot"
appId = str(uuid.uuid4())
authMethod = "token"
authToken = "7281.qwerty"

authkey = "a-kon9yz-hpnr0xv3in"
authtoken = "pnz(lna8ZZsFH8oe@M"

try:
	options = {"org": organization, "id": appId,"auth-method": "apikey", "auth-key" : authkey, "auth-token":authtoken }
except Exception as e:
	print(str(e))
	sys.exit()
	
client = ibmiotf.application.Client(options)
client.connect()

gpu_temp = 0
cpu_temp = 0

def temperaturesCallback(event):
        global gpu_temp
        global cpu_temp
        gpu_temp = event.data['GPU']
        cpu_temp = event.data['CPU']

client.deviceEventCallback = temperaturesCallback
client.subscribeToDeviceEvents(deviceType='raspi-iot-device', deviceId='rangelpi-iot', event='temperatures')

ServerApp = Flask(__name__)
@ServerApp.route('/')
def index():
    global gpu_temp
    global cpu_temp
    return render_template('index.html', cpu = cpu_temp, gpu = gpu_temp)
@ServerApp.route('/ledon/red', methods=['GET', 'POST'])
def ledonRed():
    commandData={'ledON' : 1, 'color':'red'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
@ServerApp.route('/ledoff/red', methods=['GET', 'POST'])
def ledoffRed():
    commandData={'ledON' : 0, 'color':'red'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
@ServerApp.route('/ledon/green', methods=['GET', 'POST'])
def ledonGreen():
    commandData={'ledON' : 1, 'color':'green'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
@ServerApp.route('/ledoff/green', methods=['GET', 'POST'])
def ledoffGreen():
    commandData={'ledON' : 0, 'color':'green'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
@ServerApp.route('/ledon/yellow', methods=['GET', 'POST'])
def ledonYellow():
    commandData={'ledON' : 1, 'color':'yellow'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
@ServerApp.route('/ledoff/yellow', methods=['GET', 'POST'])
def ledooffYellow():
    commandData={'ledON' : 0, 'color':'yellow'}
    client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    return redirect("/", code=302)
#client.disconnect()
if __name__ == '__main__':
    ServerApp.run(host='0.0.0.0', port=8080, debug=True)

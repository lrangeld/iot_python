import os, time, sys
import json
from os.path import join, dirname
from watson_developer_cloud import SpeechToTextV1 as SpeechToText
import pprint
import uuid
from uuid import getnode as get_mac
import ibmiotf.application
import ibmiotf.device

from stt.recorder import Recorder

organization = "kon9yz"
deviceType = "raspi-iot-device"
deviceId = "rangelpi-iot"
appId = str(uuid.uuid4())
authMethod = "token"
authToken = "7281.qwerty"

authkey = "a-kon9yz-hpnr0xv3in"
authtoken = "pnz(lna8ZZsFH8oe@M"

try:
	options = {"org": organization, "id": appId,"auth-method": "apikey", "auth-key" : authkey, "auth-token":authtoken }
except Exception as e:
	print(str(e))
	sys.exit()


def transcribe_audio(path_to_audio_file):
    speech_to_text = SpeechToText(username="eb7af922-e695-4689-b424-17324b798a77", password="kpOVFe7AMKJq")

    with open(join(dirname(__file__), path_to_audio_file), 'rb') as audio_file:
        return speech_to_text.recognize(audio_file, content_type='audio/wav', model='es-ES_NarrowbandModel')


def main():
    recorder = Recorder("audio.wav")

    print("Diga algo al microfono...\n")
    recorder.record_to_file()

    print("Transcribiendo audio....\n")
    result = transcribe_audio('audio.wav')
    
    command = result['results'][0]['alternatives'][0]['transcript']
    print("Comando: \'" + command + "\'\n")
    client = ibmiotf.application.Client(options)
    #client.connect()
    if command == 'ENCIENDE LUZ ':
            print ("Encendiendo led rojo...")
            commandData={'ledON' : 1, 'color':'red'}
            client.connect()
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'APAGA LUZ ':
            print ("Apagando led rojo...")
            client.connect()
            commandData={'ledON' : 0, 'color':'red'}
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'ENCIENDE OTRO ':
            print ("Encendiendo led amarillo...")
            commandData={'ledON' : 1, 'color':'yellow'}
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'APAGA OTRO ':
            print ("Apagando led amarillo...")
            commandData={'ledON' : 0, 'color':'yellow'}
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'ENCIENDE VENTILADOR ':
            print ("Encendiendo led verde...")
            commandData={'ledON' : 1, 'color':'green'}
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'APAGA VENTILADOR ':
            print ("Apagando led verde...")
            commandData={'ledON' : 0, 'color':'green'}
            client.publishCommand(deviceType, deviceId, "on", "json", commandData)
    if command == 'exit':
            print ("Saliendo de aplicacion...")
            wait_cmd = False
            commandData={'exiting' : 1}
            client.publishCommand(deviceType, deviceId, "exit", "json", commandData)
    client.disconnect()

if __name__ == '__main__':
    try:
        main()
    except:
        print("IOError...")
        main()



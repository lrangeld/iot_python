import time
import sys
import ibmiotf.application
import ibmiotf.device
import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(33,GPIO.OUT)
GPIO.setup(35,GPIO.OUT)
GPIO.setup(37,GPIO.OUT)

options = {
    "org" : "kon9yz",
    "type" : "raspi-iot-device",
    "id" : "rangelpi-iot",
    "auth-method" : "token",
    "auth-token" : "7281.qwerty"
}
try:
    client = ibmiotf.device.Client(options)
except Exception as e:
    print("Excepcion lanzada al intentar conectar a IBMIOTF: %s" % str(e))
    sys.exit()

wait_cmd = True
def raspiToDo(cmd):
    print("> Command received: %s" % cmd.data)
    #print("> Data ledON: %s" % cmd.data['ledON'])
    if cmd.command == "on":
        if cmd.data['ledON'] == 1 and cmd.data['color'] == 'red':
            print("Encendiendo led rojo...")
            GPIO.output(37,1)
            print("Led encendio: %s" % cmd.data['ledON'])
        elif cmd.data['ledON'] == 1 and cmd.data['color'] == 'yellow':
            print("Apagando led amarillo...")
            GPIO.output(35,1)
            print("Led Encendido: %s" % cmd.data['ledON'])
        elif cmd.data['ledON'] == 1 and cmd.data['color'] == 'green':
            print("Apagando led verde...")
            GPIO.output(33,1)
            print("Led Encendido: %s" % cmd.data['ledON'])
        elif cmd.data['ledON'] == 0 and cmd.data['color'] == 'red':
            print("Apagando led rojo...")
            GPIO.output(37,0)
            print("Led Apagado: %s" % cmd.data['ledON'])
        elif cmd.data['ledON'] == 0 and cmd.data['color'] == 'yellow':
            print("Apagando led amarillo...")
            GPIO.output(35,0)
            print("Led Apagado: %s" % cmd.data['ledON'])
        elif cmd.data['ledON'] == 0 and cmd.data['color'] == 'green':
            print("Apagando led verde...")
            GPIO.output(33,0)
            print("Led Apagado: %s" % cmd.data['ledON'])
        else:
            print("Que sad...")
    if cmd.command == "exit":
        if cmd.data['exiting'] == 1:
            print("Cerrando cliente...")
            wait_cmd = False
            print("Exiting: %s" % cmd.data['exiting'])


client.connect()
client.commandCallback = raspiToDo
while(wait_cmd):
        print("esperando comando...")
        time.sleep(2)
client.disconnect()

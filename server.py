import time
import sys
import json
import pprint
import uuid
from uuid import getnode as get_mac
import ibmiotf.application
import ibmiotf.device
    
organization = "kon9yz"
deviceType = "raspi-iot-device"
deviceId = "rangelpi-iot"
appId = str(uuid.uuid4())
authMethod = "token"
authToken = "7281.qwerty"

authkey = "a-kon9yz-hpnr0xv3in"
authtoken = "pnz(lna8ZZsFH8oe@M"

try:
	options = {"org": organization, "id": appId,"auth-method": "apikey", "auth-key" : authkey, "auth-token":authtoken }
except Exception as e:
	print(str(e))
	sys.exit()
wait_cmd = True
while(wait_cmd):
        command = input("Ingrese comando: ")
        #client = ibmiotf.application.Client(options)
        #client.connect()
        if command == 'ron':
                print ("Encendiendo led rojo...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 1, 'color':'red'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'roff':
                print ("Apagando led rojo...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 0, 'color':'red'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'yon':
                print ("Encendiendo led amarillo...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 1, 'color':'yellow'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'yoff':
                print ("Apagando led amarillo...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 0, 'color':'yellow'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'gon':
                print ("Encendiendo led verde...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 1, 'color':'green'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'goff':
                print ("Apagando led verde...")
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'ledON' : 0, 'color':'green'}
                client.publishCommand(deviceType, deviceId, "on", "json", commandData)
        if command == 'exit':
                print ("Saliendo de aplicacion...")
                wait_cmd = False
                client = ibmiotf.application.Client(options)
                client.connect()
                commandData={'exiting' : 1}
                client.publishCommand(deviceType, deviceId, "exit", "json", commandData)
client.disconnect()
